﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject Player1, Player2;

    public Image P1Filler,P2Filler;

    public static Image P1HealsFiller,P2HealsFiller;

    private static int RandomColors=0;

    public static Color colors;

    public static float k=2.0f;

    void Start()
    {
        P1HealsFiller=P1Filler;
        P2HealsFiller=P2Filler;
    }

    //Player random color function
    public static void RandomColor(GameObject objects)
    {
        RandomColors= Random.Range(1, 4);
        if (RandomColors == 1)
        {
            colors = new Color(1f, 0f, 0, 1);
        }

        if (RandomColors == 2)
        {
            colors = new Color(0, 1, 0, 1);
        }

        if (RandomColors == 3)
        {
            colors = new Color(0, 0, 1, 1);
        }

            objects.GetComponent<SpriteRenderer>().color = colors;

    }
    //Button events (player color changing)
    public void ButtonP1Red()
    {
        Player1.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1);
    }

    public void ButtonP1Green()
    {
        Player1.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 1);
    }

    public void ButtonP1Blue()
    {
        Player1.GetComponent<SpriteRenderer>().color = new Color(0, 0, 1, 1);
    }

    public void ButtonP2Red()
    {
        Player2.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1);
    }

    public void ButtonP2Green()
    {
        Player2.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 1);
    }

    public void ButtonP2Blue()
    {
        Player2.GetComponent<SpriteRenderer>().color = new Color(0, 0, 1, 1);
    }

    //HP bars filler
    public static void FillAmountChange()
    {
        P1HealsFiller.fillAmount = (float)Player.tHP1 / 100.0f;
        P2HealsFiller.fillAmount = (float)Player.tHP2 / 100.0f;
    }
}
