﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float x, y;

    public float speed = 0.1f;

    Rigidbody2D clone;

    public static void spawnBullet(GameObject prefab, float x, float y)
    {
        Instantiate(prefab, new Vector2(x, y), Quaternion.identity);
    }

    void Start()
    {
        clone = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        speed += 0.01f * UIController.k;

        x = this.gameObject.transform.position.x + 1;
        y = this.gameObject.transform.position.y;

        if (gameObject.name == "P1Bullet(Clone)")
        {
            clone.velocity = new Vector2(speed, 0f);
        }

        if (gameObject.name == "P2Bullet(Clone)")
        {
            clone.velocity = new Vector2(-1 * speed, 0f);
        }
    }

    public void OnTriggerEnter2D(Collider2D PlayerCol)
    {
        if (PlayerCol.gameObject.name == "Player1")
        {
            UIController.k += 0.2f;

            if (PlayerCol.GetComponent<SpriteRenderer>().color==this.GetComponent<SpriteRenderer>().color)
            {
                Debug.Log("Damaga net");  
            }

            else

            {
                Player.HPCalculating1();
                UIController.FillAmountChange();
            }

            Destroy(gameObject);

            Player.resp2 = true;
        }

        if (PlayerCol.gameObject.name == "Player2")
        {
             if(PlayerCol.GetComponent<SpriteRenderer>().color==this.GetComponent<SpriteRenderer>().color)
            {
                Debug.Log("Damaga net2");  
            }

            else

            {
                Player.HPCalculating2();
                UIController.FillAmountChange();
            }

            Destroy(gameObject);

            Player.resp1 = true;
        }
    }
}