﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private int maxHP = 100;

    public static bool resp1, resp2;

    //public static int tHPs;

    public static int tHP1,tHP2;

    public GameObject Player1, Player2, prefab1, prefab2;

    public static void HPCalculating1()
    {
        tHP1 = tHP1 - 20;
    }

    public static void HPCalculating2()
    {
        tHP2 = tHP2 - 20;
    }

    /*public static int HPCalculating(int tHP)
    {
        return (tHP - Random.Range(0, 20));
    }*/

    void Start()
    {
        tHP1 = maxHP;
        tHP2 = maxHP;

        resp1 = false;
        resp2 = false;

        UIController.RandomColor(Player1);
        UIController.RandomColor(Player2);

        FirstSpawnBullet1();

        FirstSpawnBullet2();
    }

    void Update()
    {
        if (resp1==true) 
        {
            resp1 = false;

            FirstSpawnBullet1();
        }

        if (resp2 == true)
        {
            resp2 = false;

            FirstSpawnBullet2();
        }
    }

        public void FirstSpawnBullet1()
    {
        Bullet.spawnBullet(prefab1, -5f, 0.5f);

        UIController.RandomColor(prefab1);
    }

        public void FirstSpawnBullet2()
    {
        Bullet.spawnBullet(prefab2, 5f, -0.75f);

        UIController.RandomColor(prefab2);
    }
}